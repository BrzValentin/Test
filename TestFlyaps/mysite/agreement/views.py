from .models import Agreement, Company, Country, Period, Negotiator
from .serializers import AgreementSerializer, CompanySerializer, CountrySerializer, NegotiatorSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from django.contrib.auth.models import User
from django.shortcuts import render
from rest_framework.decorators import api_view
from django.http import HttpResponse

def func(request):
    return render(request, 'agreement/href.html')

class AgreementList(APIView):
    def get(self, request, format=None):
        agreements = Agreement.objects.all()
        serializer = AgreementSerializer(agreements, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = AgreementSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class AgreementDetail(APIView):
    def get_object(self, pk):
        try:
            return Agreement.objects.get(pk=pk)
        except Agreement.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        agreement = self.get_object(pk)
        serializer = AgreementSerializer(agreement)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        agreement = self.get_object(pk)
        serializer = AgreementSerializer(agreement, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        agreement = self.get_object(pk)
        agreement.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

        company = Company.objects.all()
        serializercompany = CompanySerializer(company, many=True)
        country = Country.objects.all()
        serializercountry = CountrySerializer(country, many=True)
        negotiator = Negotiator.objects.all()
        serializernegotiator = NegotiatorSerializer(negotiator, many=True)

@api_view()
def calendar(request):
    country = request.GET.get('country')
    negotiator = request.GET.get('negotiator')
    company = request.GET.get('company')

    agreements_id = [agr.id for agr in Agreement.objects.all()]
    periods = [obj for obj in Period.objects.all() ]

    result={}
    if country == None and negotiator == None and company == None:
        l_indexs_the_last_periods_for_each_agreements  = __find_the_last_periods__(agreements_id, periods)
        l_end_periods_in_agreements = [periods[index] for index in l_indexs_the_last_periods_for_each_agreements]

        for obj in l_end_periods_in_agreements:
            end_date = str(obj.end_date)
            year = end_date[0:4]
            if year not in result:
                l_agreemets = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                l_agreemets[int(end_date[5:7]) - 1] += 1
                result.update({year: l_agreemets})
            else:
                l_agreemets = result.get(year)
                l_agreemets[int(end_date[5:7]) - 1] += 1
                result.update({year: l_agreemets})          
    else:
        agreements_country_id, agreements_negotiator_id, agreements_company_id = [], [], []
        if country:
            list_countrys = [int(x) for x in country.split(',')]
            agreements_country_id = [obj.id for country in list_countrys for obj in Agreement.objects.filter(company__country__name = Country.objects.get(pk=country).name)]

        if negotiator:
            list_negotiators = [int(x) for x in negotiator.split(',')]
            agreements_negotiator_id = [obj.id for negotiator in list_negotiators for obj in Agreement.objects.filter(negotiator__negotiator__username = User.objects.get(pk=negotiator).username)]
        
        if company:
            list_companys = [int(x) for x in company.split(',')]
            agreements_company_id = [obj.id for company in list_companys for obj in Agreement.objects.filter(company__name = Company.objects.get(pk=company).name)]

        agreements_id = agreements_country_id + agreements_negotiator_id + agreements_company_id
        main_ids = []

        if (company and negotiator) or (company and country) or (negotiator and country):
            main_ids = [id for id in agreements_id if agreements_id.count(id) == 2]

        if company and country == None and negotiator == None:
            main_ids = [id for id in agreements_id]

        if company == None and country and negotiator == None:
            main_ids = [id for id in agreements_id]

        if company == None and country == None and negotiator:
            main_ids = [id for id in agreements_id]

        main_ids = set(main_ids)

        l_index_the_last_periods = __find_the_last_periods__(main_ids, periods)
        l_end_periods_in_agreements = [periods[index] for index in l_index_the_last_periods]

        for obj in l_end_periods_in_agreements:
            for agreement_id in main_ids:
                if obj.agreement.id == agreement_id:
                    end_date = str(obj.end_date)
                    year = end_date[0:4]
                    if year not in result:
                        l_agreemets = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                        l_agreemets[int(end_date[5:7]) - 1] += 1
                        result.update({year: l_agreemets})
                    else:
                        l_agreemets = result.get(year)
                        l_agreemets[int(end_date[5:7]) - 1] += 1
                        result.update({year: l_agreemets})

    return Response(result)

def __find_the_last_periods__(agreements_id , periods):
    l_indexs_the_last_periods = []
    last_index = -1
    for agr_id in agreements_id:
        for i in range(len(periods)):
            if agr_id == periods[i].agreement.id:
                last_index = i
        l_indexs_the_last_periods.append(last_index)
    return l_indexs_the_last_periods     
