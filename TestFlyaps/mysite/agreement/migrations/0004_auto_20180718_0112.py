# -*- coding: utf-8 -*-
# Generated by Django 1.11.14 on 2018-07-17 22:12
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agreement', '0003_dron'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dron',
            name='name',
            field=models.CharField(max_length=100),
        ),
    ]
