from django.db import models
#from django.contrib import messages
from rest_framework import status
from rest_framework.response import Response

# Create your models here.
from django.db import models
from django.contrib.auth.models import User

class Country(models.Model):
    code = models.CharField(max_length=3)
    name = models.CharField(max_length=100)

class Company(models.Model):
    name = models.CharField(max_length=100)
    country = models.ForeignKey(Country, on_delete=models.CASCADE)

class Negotiator(models.Model):
    negotiator = models.ForeignKey(User)

class Agreement(models.Model):
    begin_date = models.DateField()
    end_date = models.DateField()
    company = models.ForeignKey(Company, default=1, on_delete=models.CASCADE)
    negotiator = models.ForeignKey(Negotiator, on_delete=models.CASCADE)
    credit = models.DecimalField(max_digits=11, decimal_places=2)
    debit = models.DecimalField(max_digits=11, decimal_places=2)

    def save(self, *args, **kwargs):
        if self.begin_date < self.end_date:
            super().save(*args, **kwargs)
        # else:
        #     Response(status=status.HTTP_409_CONFLICT)

class Period(models.Model):
    begin_date = models.DateField()
    end_date = models.DateField()
    choices_status = (
        ('New','New'),
        ('Act','Active'),
        ('Rec','Reconciliation'),
        ('Cl','Closed'),
    )
    status = models.CharField(max_length=20, choices=choices_status)
    agreement = models.ForeignKey(Agreement, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
            if self.begin_date < self.end_date:
                periods = Period.objects.filter(agreement = self.agreement)
                if len(periods) != 0:
                    count = 0
                    for obj in periods:
                        if self.begin_date < obj.begin_date and self.end_date < obj.begin_date:
                            count += 1
                        if self.begin_date > obj.begin_date and self.begin_date > obj.end_date and self.end_date > obj.end_date:
                            count += 1
                    if count == len(periods):
                        super().save(*args, **kwargs)
                else:
                    super().save(*args, **kwargs)


# Create your models here.

