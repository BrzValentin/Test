from rest_framework import serializers
from .models import Agreement, Company, Country, Negotiator, Period

class AgreementSerializer(serializers.ModelSerializer):
    class Meta:
        model = Agreement
        fields = ('id', 'begin_date','end_date','company','negotiator','credit', 'debit')

class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ('id', 'name', 'country')

class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = ('id', 'code', 'name')

class NegotiatorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Negotiator
        fields = ('id', 'negotiator')

class PeriodSerializer(serializers.ModelSerializer):
    class Meta: 
        model = Period
        fields = ('','')