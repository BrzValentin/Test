from django.contrib import admin
from .models import Period, Agreement, Company, Country, Negotiator

@admin.register(Period)
class PeriodAdmin(admin.ModelAdmin):
    list_display = ('status',)
    list_filter = ('begin_date', 'end_date')
    search_fields = ['begin_date', 'end_date']

class PeriodInline(admin.TabularInline):
    model = Period

@admin.register(Agreement)
class AgreementAdmin(admin.ModelAdmin):
    list_display = ('begin_date', 'end_date','negotiator')
    search_fields = ['negotiator', 'begin_date', 'end_date']
    list_filter = ('begin_date', 'end_date','negotiator')
    inlines = [
        PeriodInline,
    ]

@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    list_display = ['name', 'code']
    search_fields = ['name', 'code']
    list_filter = ('code',)

@admin.register(Company)
class Company(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ['name',]
    list_filter = ('name',)

@admin.register(Negotiator)
class Negotiator(admin.ModelAdmin):
    search_fields = ('negotiator',)

#admin.site.register(Agreement)
# Register your models here.